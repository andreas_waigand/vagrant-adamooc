# Prerequisites
You need to install vagrant and virtualbox.

# Usage

1. Download the repo
2. Navigate to the repo
3. `vagrant up`
4. `vagrant ssh`
5. `cd /vagrant`
6. `./install.sh`

You can then config HONE to use this virtual server by changing the PouchLoginService  `host` and `signUpHost` variable to `127.0.0.1:8080` and `127.0.0.1:8081` respectively.
To use the flashcards plugin against the database you need to change PouchStartupService `host` to `127.0.0.1:48080`

If you do this on windows you might have t ochange the linending in the install.sh to LF